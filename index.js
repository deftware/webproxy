const https = require('https');
const http = require('http');
const httpProxy = require('http-proxy');
const fs = require('fs');
const nconf = require('nconf');

if (!fs.existsSync('./config.json')) {
    fs.writeFileSync('./config.json', JSON.stringify({
        serverType: "http",
        port: 8080,
        proxies: []
    }));
}

nconf.file({
    file: './config.json'
});

const cert = fs.existsSync('./cert/cert.pem') ? {
    key: fs.readFileSync('./cert/rsa.key'),
    cert: fs.readFileSync('./cert/cert.pem')
} : null;

const proxy = httpProxy.createProxyServer({
    changeOrigin: true
});

let proxies = {};

function handleWebReq(req, res) {
    let host = `${req.headers.host}`;
    if (host === undefined || host === "" || !proxies[host]) {
        return res.end(`Could not find proxy for site ${host}`);
    } 
    proxies[host](req, res);
}

if (nconf.get("proxies").length === 0) {
    console.warn("Please configure proxies in the config.json");
} else {
    nconf.get("proxies").forEach(p => {
        proxies[`${p.externalHost}:${nconf.get("port")}`] = (req, res) => {
            proxy.web(req, res, {
                target: `${p.ssl ? "https" : "http"}://${p.host}:${p.port}`,
                secure: p.ssl
            });
        }
    });
    if (nconf.get("serverType") === "ssl") {
        if (cert === null) {
            return console.warn("Could not find SSL certificates in ./cert/");
        } else {
            https.createServer(cert, handleWebReq).listen(nconf.get("port"));
        }
    } else {
        http.createServer(handleWebReq).listen(nconf.get("port"));
    }
    console.log(`Proxy server started on port ${nconf.get("port")}`);
}

